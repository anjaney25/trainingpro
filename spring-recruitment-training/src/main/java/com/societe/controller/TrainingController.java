package com.societe.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.societe.exception.CourseNotFoundException;
import com.societe.exception.EmployeeNotFoundException;
import com.societe.exception.ProfileNotFoundException;
import com.societe.model.Course;
import com.societe.model.Employee;
import com.societe.model.Job;
import com.societe.model.Profile;
import com.societe.model.Program;
import com.societe.model.Skills;
import com.societe.service.AssignCourse;
import com.societe.service.CourseService;
import com.societe.service.EmployeeService;
import com.societe.service.ProfileService;
import com.societe.service.ProgramService;
import com.societe.service.SkillsService;

@RestController
@RequestMapping("/training-api")
public class TrainingController {
@Autowired
EmployeeService employeeService;
@Autowired
ProfileService profileService;
@Autowired
CourseService courseService;
@Autowired
ProgramService programService;
@Autowired
SkillsService skillService;
@Autowired
AssignCourse assignCourse;

@Autowired
RestTemplate restTemplate;



@GetMapping("/save/allEmployees")
public void saveAllEmployees() {
	String url = "/";
	List<Employee> employeeList = restTemplate.getForObject(url, List.class);

	for (Employee employee : employeeList) {
		employeeService.addEmployee(employee);
	}
	
}

@GetMapping("/save/profile")
public void saveProfile() {
	String url = "/";
	Job job = restTemplate.getForObject(url, Job.class );
	
	Profile profile = new Profile();
	profile.setProfileId(job.getJobId());
	profile.setProfileName(job.getProfile());
	profile.setSkillSet(job.getSkillSet());
	profileService.addProfile(profile);
	

}

@GetMapping("/get-all-courses")
public List<Course> getAllCourses() {
	return courseService.getAllCourses();
}

@GetMapping("/courses/program/{programName}")
public List<Course> getCoursesbyProgram(@PathVariable String programName){
	Program program = programService.getProgrambyName(programName);
	return courseService.findByProgram(program);
	
}

@GetMapping("/course/courseId/{courseId}")
public Course getCoursebyId(@PathVariable Integer courseId) {
	Course course = new Course();
	try {
		course = courseService.getCoursebyId(courseId);
	} catch (CourseNotFoundException e) {
		e.printStackTrace();
	}
	return course;
}

@GetMapping("/get-all-employees")
public List<Employee> getAllEmployees() {
	return employeeService.getAllEmployees();
}

@GetMapping("/employees/profile/{profileName}")
public List<Employee> getEmployeesbyProfile(@PathVariable String profileName) throws ProfileNotFoundException{
	return employeeService.getEmployeebyProfile(profileName);
	
}

@GetMapping("/employee/empId/{empId}")
public Employee getEmployeebyId(@PathVariable Integer empId)  {
	Employee employee = new Employee();
	try {
		employee =  employeeService.getEmployeebyId(empId);
	} catch (EmployeeNotFoundException e) {
		e.printStackTrace();
	}
	return employee;
}

@GetMapping("/get-all-profiles")
public List<Profile> getAllProfiles() {
	return profileService.getAllProfiles();
}

@GetMapping("/profile/profileId/{profileId}")
public Profile getProfilebyId(@PathVariable Integer profileId) {
	Profile profile = new Profile();
	try {
		profile =  profileService.getProfilebyId(profileId);
	} catch (ProfileNotFoundException e) {
		e.printStackTrace();
	}
	return profile;
}

@GetMapping("/profile/profileName/{profileName}")
public Profile getProfilebyName(@PathVariable String profileName) throws ProfileNotFoundException {
	return profileService.getProfilebyName(profileName);
}

@GetMapping("/get-all-programs")
public List<Program> getAllPrograms() {
	return programService.getAllPrograms();
}

@GetMapping("/get-all-skills")
public List<Skills> getAllSkills() {
	return skillService.getAllSkills();
}

@GetMapping("/assign-courses-training/employeeId/{empId}")
public Set<Course> assignCourses(@PathVariable Integer empId){
	return assignCourse.assignCourses(empId);
}

@GetMapping("/assign-courses-training")
public Set<Set<Course>> assignCourses(){
	List<Employee> employeeList = employeeService.getAllEmployees();
	Set<Set<Course>> trainingList = new HashSet<>();
	for (Employee employee : employeeList) {
		trainingList.add(assignCourse.assignCourses(employee.getEmpId()));
	}
	return trainingList;
	
}






}
