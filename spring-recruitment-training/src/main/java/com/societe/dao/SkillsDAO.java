package com.societe.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.societe.model.Skills;
@Repository
public interface SkillsDAO extends JpaRepository<Skills, Integer> {

}
