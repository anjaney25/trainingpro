package com.societe.service;

import java.util.List;

import com.societe.model.Skills;

public interface SkillsService {

	void addSkill(Skills skill);

	List<Skills> getAllSkills();

}
