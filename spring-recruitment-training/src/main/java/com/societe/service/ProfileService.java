package com.societe.service;

import java.util.List;

import com.societe.exception.ProfileNotFoundException;
import com.societe.model.Profile;

public interface ProfileService {

	void addProfile(Profile profile);

	List<Profile> getAllProfiles();

	Profile getProfilebyId(Integer profileId) throws ProfileNotFoundException;

	Profile getProfilebyName(String profileName) throws ProfileNotFoundException;

}
