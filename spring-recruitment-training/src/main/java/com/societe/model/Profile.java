package com.societe.model;

import java.util.Set;

import javax.persistence.CascadeType;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Profile {
	@Id
	
	private Integer profileId;
	
	private String profileName;
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "PROFILE_SKILLS", joinColumns = { @JoinColumn(name = "PROFILE_ID") }, inverseJoinColumns = {
			@JoinColumn(name = "SKILL_ID") })
	private Set<Skills> skillSet;

	public Profile() {
		super();
	}

	public Integer getProfileId() {
		return profileId;
	}

	public void setProfileId(Integer profileId) {
		this.profileId = profileId;
	}

	public String getProfileName() {
		return profileName;
	}

	public void setProfileName(String profileName) {
		this.profileName = profileName;
	}

	public Set<Skills> getSkillSet() {
		return skillSet;
	}

	public void setSkillSet(Set<Skills> skillSet) {
		this.skillSet = skillSet;
	}

	@Override
	public String toString() {
		return "Profile [profileId=" + profileId + ", profileName=" + profileName + ", skillSet=" + skillSet + "]";
	}

	
}
