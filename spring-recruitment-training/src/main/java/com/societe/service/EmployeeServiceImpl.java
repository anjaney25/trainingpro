package com.societe.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.societe.dao.EmployeeDAO;
import com.societe.exception.EmployeeNotFoundException;
import com.societe.exception.ProfileNotFoundException;
import com.societe.model.Employee;

@Service
public class EmployeeServiceImpl implements EmployeeService {
	@Autowired
	EmployeeDAO employeeDAO;

	@Override
	public void addEmployee(Employee employee) {
		employeeDAO.save(employee);

	}

	@Override
	public void deleteEmployee(Employee employee) {
		employeeDAO.delete(employee);

	}

	@Override
	public List<Employee> getAllEmployees() {
		return employeeDAO.findAll();
	}

	@Override
	public Employee getEmployeebyId(Integer empId) throws EmployeeNotFoundException {
		return employeeDAO.getOne(empId);
	}

	@Override
	public List<Employee> getEmployeebyProfile(String profile) throws ProfileNotFoundException {
		return employeeDAO.findbyProfile(profile);
	}

}
