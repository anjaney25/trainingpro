package com.societe.service;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.societe.exception.EmployeeNotFoundException;
import com.societe.exception.ProfileNotFoundException;
import com.societe.model.Course;
import com.societe.model.Employee;
import com.societe.model.Profile;
import com.societe.model.Skills;

@Service
public class AssignCourse {

	@Autowired
	ProfileService profileService;
	@Autowired
	EmployeeService employeeService;
	@Autowired
	CourseService courseService;

	public Set<Course> assignCourses(Integer empId) {
		Employee employee = new Employee();
		try {
			employee = employeeService.getEmployeebyId(empId);
		} catch (EmployeeNotFoundException e) {
			e.printStackTrace();
		}

		String profileName = employee.getProfile();
		Profile profile = new Profile();
		try {
			profile = profileService.getProfilebyName(profileName);
		} catch (ProfileNotFoundException e) {
			e.printStackTrace();
		}

		Set<Skills> skillRequired = profile.getSkillSet();
		Set<Skills> skillPresent = employee.getSkills();
		Set<Course> courseAssign = new HashSet<>();

		for (Skills skillProfile : skillRequired) {
			boolean status = true;
			String skillName = skillProfile.getSkillName();
			for (Skills skillEmployee : skillPresent) {
				if (skillName.equals(skillEmployee.getSkillName())) {
					status = false;
					break;

				}
			}
			if (status == true) {
				Course course = courseService.getCoursebyName(skillName);
				courseAssign.add(course);
			}

		}

		return courseAssign;

	}
}
