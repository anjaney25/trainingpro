package com.societe.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;

import com.societe.model.Program;

@Repository
public interface ProgramDAO extends JpaRepository<Program, Integer> {

	Program findByProgramName(String programName);

}
