package com.societe.model;


import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Trainer {
	
	private String trainerName;
	@Id
	
	private Integer trainerId;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "trainer")
	@JsonIgnore
	private Set<Course> courseList ;

	public Trainer() {
		super();
	}

	public String getTrainerName() {
		return trainerName;
	}

	public void setTrainerName(String trainerName) {
		this.trainerName = trainerName;
	}

	public Integer getTrainerId() {
		return trainerId;
	}

	public void setTrainerId(Integer trainerId) {
		this.trainerId = trainerId;
	}

	public Set<Course> getCourseList() {
		return courseList;
	}

	public void setCourseList(Set<Course> courseList) {
		this.courseList = courseList;
	}

	@Override
	public String toString() {
		return "Trainer [trainerName=" + trainerName + ", trainerId=" + trainerId + "]";
	}

}
