package com.societe.service;

import java.util.List;

import com.societe.exception.EmployeeNotFoundException;
import com.societe.exception.ProfileNotFoundException;
import com.societe.model.Employee;

public interface EmployeeService {

	void addEmployee(Employee employee);

	void deleteEmployee(Employee employee);

	List<Employee> getAllEmployees();

	Employee getEmployeebyId(Integer empId) throws EmployeeNotFoundException;

	List<Employee> getEmployeebyProfile(String profile) throws ProfileNotFoundException;

}
