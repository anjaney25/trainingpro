package com.societe.exception;

public class ProfileNotFoundException extends Exception {

	private static final long serialVersionUID = 1L;

	public ProfileNotFoundException() {
		super();
		
	}

	public ProfileNotFoundException(String message) {
		super(message);
		
	}

	
}
