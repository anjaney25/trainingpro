package com.societe.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.societe.dao.ProgramDAO;
import com.societe.model.Program;

@Service
public class ProgramServiceImpl implements ProgramService {

	@Autowired
	ProgramDAO programDAO;

	@Override
	public Program getProgrambyName(String programName) {

		return programDAO.findByProgramName(programName);
	}

	@Override
	public Program getProgrambyId(Integer programId) {
		return programDAO.getOne(programId);
	}

	@Override
	public List<Program> getAllPrograms() {
		return programDAO.findAll();
	}

}
