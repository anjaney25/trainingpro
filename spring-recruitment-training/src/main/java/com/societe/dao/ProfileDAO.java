package com.societe.dao;




import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;


import com.societe.model.Profile;
@Repository
public interface ProfileDAO extends JpaRepository<Profile, Integer> {

	Profile findByProfileName(String profileName);


	
}
