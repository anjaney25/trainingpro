package com.societe.model;

import javax.persistence.Embeddable;

@Embeddable
public class ProgramManager {

	private String managerName;
	private Integer managerId;

	public String getName() {
		return managerName;
	}

	public void setName(String name) {
		this.managerName = name;
	}

	public Integer getId() {
		return managerId;
	}

	public void setId(Integer id) {
		this.managerId = id;
	}

	@Override
	public String toString() {
		return "ProgramManager [name=" + managerName + ", id=" + managerId + "]";
	}

}
