package com.societe.service;

import java.util.List;

import com.societe.exception.CourseNotFoundException;
import com.societe.model.Course;
import com.societe.model.Program;

public interface CourseService {

	void addCourse(Course course);

	void update(Course course);

	Course getCoursebyId(Integer courseId) throws CourseNotFoundException;
	Course getCoursebyName(String courseName);

	List<Course> getAllCourses();

	List<Course> findByProgram(Program program);

}
