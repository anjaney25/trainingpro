package com.societe.model;


import java.util.Set;

import javax.persistence.CascadeType;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Skills {
	
	private String skillName;
	@Id
	
	private Integer skillId;
	@ManyToMany(cascade = { CascadeType.ALL },

			mappedBy = "skills")
	@JsonIgnore
	private Set<Employee> employeeList ;
	
	@ManyToMany(cascade = { CascadeType.ALL },

			mappedBy = "skillSet")
	@JsonIgnore
	private Set<Profile> profileList ;

	public Skills() {
		super();
	}

	public String getSkillName() {
		return skillName;
	}

	public void setSkillName(String skillName) {
		this.skillName = skillName;
	}

	public Integer getSkillId() {
		return skillId;
	}

	public void setSkillId(Integer skillId) {
		this.skillId = skillId;
	}

	public Set<Employee> getEmployeeList() {
		return employeeList;
	}

	public void setEmployeeList(Set<Employee> employeeList) {
		this.employeeList = employeeList;
	}

	public Set<Profile> getProfileList() {
		return profileList;
	}

	public void setProfileList(Set<Profile> profileList) {
		this.profileList = profileList;
	}

	@Override
	public String toString() {
		return "Skills [skillName=" + skillName + ", skillId=" + skillId + ", employeeList=" + employeeList
				+ ", profileList=" + profileList + "]";
	}

	
}
