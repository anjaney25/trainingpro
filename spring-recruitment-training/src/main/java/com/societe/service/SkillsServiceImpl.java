package com.societe.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.societe.dao.SkillsDAO;
import com.societe.model.Skills;

@Service
public class SkillsServiceImpl implements SkillsService {
	@Autowired
	SkillsDAO skillsDAO;

	@Override
	public void addSkill(Skills skill) {
		skillsDAO.save(skill);

	}

	@Override
	public List<Skills> getAllSkills() {
		return skillsDAO.findAll();
	}

}
