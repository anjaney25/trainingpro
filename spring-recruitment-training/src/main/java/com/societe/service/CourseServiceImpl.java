package com.societe.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.societe.dao.CourseDAO;
import com.societe.exception.CourseNotFoundException;
import com.societe.model.Course;
import com.societe.model.Program;

@Service
public class CourseServiceImpl implements CourseService {

	@Autowired
	CourseDAO courseDAO;

	@Override
	public void addCourse(Course course) {

		courseDAO.save(course);
	}

	@Override
	public void update(Course course) {
		courseDAO.save(course);

	}

	@Override
	public Course getCoursebyId(Integer courseId) throws CourseNotFoundException {
		return courseDAO.getOne(courseId);

	}

	@Override
	public List<Course> getAllCourses() {
		return courseDAO.findAll();
	}

	@Override
	public List<Course> findByProgram(Program program) {
		return courseDAO.findByProgram(program);
	}

	@Override
	public Course getCoursebyName(String courseName) {
		return courseDAO.findByCourseName(courseName);
	}

}
