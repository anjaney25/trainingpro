package com.societe.model;

import java.util.Set;

public class Job {
	private String title;
	private Integer jobId;
	private String profile;
	private String dept;
	private Integer experience;
	private String type;
	private String location;
	private Set<Skills> skillSet;
	private Integer vacancy;

	public Job() {
		super();
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getJobId() {
		return jobId;
	}

	public void setJobId(Integer jobId) {
		this.jobId = jobId;
	}

	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

	public String getDept() {
		return dept;
	}

	public void setDept(String dept) {
		this.dept = dept;
	}

	public Integer getExperience() {
		return experience;
	}

	public void setExperience(Integer experience) {
		this.experience = experience;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Set<Skills> getSkillSet() {
		return skillSet;
	}

	public void setSkillSet(Set<Skills> skillSet) {
		this.skillSet = skillSet;
	}

	public Integer getVacancy() {
		return vacancy;
	}

	public void setVacancy(Integer vacancy) {
		this.vacancy = vacancy;
	}

	@Override
	public String toString() {
		return "Job [title=" + title + ", jobId=" + jobId + ", profile=" + profile + ", dept=" + dept + ", experience="
				+ experience + ", type=" + type + ", location=" + location + ", skillSet=" + skillSet + ", vacancy="
				+ vacancy + "]";
	}

	public Job(String title, Integer jobId, String profile, String dept, Integer experience, String type,
			String location, Set<Skills> skillSet, Integer vacancy) {
		super();
		this.title = title;
		this.jobId = jobId;
		this.profile = profile;
		this.dept = dept;
		this.experience = experience;
		this.type = type;
		this.location = location;
		this.skillSet = skillSet;
		this.vacancy = vacancy;
	}

}