package com.societe.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.societe.dao.ProfileDAO;
import com.societe.exception.ProfileNotFoundException;
import com.societe.model.Profile;

@Service
public class ProfileServiceImpl implements ProfileService {

	@Autowired
	ProfileDAO profileDAO;

	@Override
	public void addProfile(Profile profile) {
		profileDAO.save(profile);

	}

	@Override
	public List<Profile> getAllProfiles() {
		return profileDAO.findAll();
	}

	@Override
	public Profile getProfilebyId(Integer profileId) throws ProfileNotFoundException {
		return profileDAO.getOne(profileId);
	}

	@Override
	public Profile getProfilebyName(String profileName) throws ProfileNotFoundException {
		return profileDAO.findByProfileName(profileName);
	}

}
