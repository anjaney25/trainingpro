package com.societe.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.societe.model.Employee;

@Repository
public interface EmployeeDAO extends JpaRepository<Employee, Integer> {

	@Query("Select e from Employee e where profile=?1")
	List<Employee> findbyProfile(String profile);

}
