package com.societe.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;

import com.societe.model.Course;

import com.societe.model.Program;
@Repository
public interface CourseDAO extends JpaRepository<Course, Integer> {

	List<Course> findByProgram(Program program);

	Course findByCourseName(String courseName);

	
	
}
