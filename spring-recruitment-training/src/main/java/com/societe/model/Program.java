package com.societe.model;


import java.util.Set;

import javax.persistence.CascadeType;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Program {
	@Id
	
	private Integer programId;
	
	private String programName;
	@Embedded
	private ProgramManager programManager;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "program")
	@JsonIgnore
	private Set<Course> courseList ;

	public Program() {
		super();
	}

	public Integer getProgramId() {
		return programId;
	}

	public void setProgramId(Integer programId) {
		this.programId = programId;
	}

	public String getProgramName() {
		return programName;
	}

	public void setProgramName(String programName) {
		this.programName = programName;
	}

	public ProgramManager getProgramManager() {
		return programManager;
	}

	public void setProgramManager(ProgramManager programManager) {
		this.programManager = programManager;
	}

	public Set<Course> getCourseList() {
		return courseList;
	}

	public void setCourseList(Set<Course> courseList) {
		this.courseList = courseList;
	}

	@Override
	public String toString() {
		return "Program [programId=" + programId + ", programName=" + programName + ", programManager=" + programManager
				+  "]";
	}

}
