package com.societe.service;

import java.util.List;

import com.societe.model.Program;

public interface ProgramService {

	Program getProgrambyName(String programName);
	Program getProgrambyId(Integer programId);
	List<Program> getAllPrograms();
}
